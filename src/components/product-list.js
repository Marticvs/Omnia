import React from 'react';
import { Card } from 'semantic-ui-react';
import ProductCard from './product-card';

export default function ProductList({products, deleteProduct}){

  const cards = () => {
    return products.map(product => {
      return (
        <ProductCard key={product._id}
        product={product}
        deleteProduct={deleteProduct} />
      )
    })
  }

  return (
    <Card.Group>
      { cards() }
    </Card.Group>
  )
}