import React from 'react';
import { Link } from 'react-router-dom';
import { Card, Button, Icon } from 'semantic-ui-react';

export default function ProductCard({product, deleteProduct}) {
  return (
    <Card>
      <Card.Content>
        <Card.Header>
          <Icon name='user outline'/> {product.name.first} {product.name.last}
        </Card.Header>
        <Card.Description>
          <p><Icon name='phone'/> {product.phone}</p>
          <p><Icon name='mail outline'/> {product.email}</p>
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <div className="ui two buttons">
        <Link to={`/products/edit/${product._id}`} className="ui basic button green">Edit</Link>
          <Button basic color="red" onClick={() => deleteProduct(product._id)} >Delete</Button>
      </div>
      </Card.Content>
    </Card>
  )
}

ProductCard.propTypes = {
  product: React.PropTypes.object.isRequired
}