export const products = [
  {
    _id: "1",
    name: {
      first:"Iphone",
      last:"6"
    },
    phone:"555",
    email:"john@gmail.com"
  },
  {
    _id: "2",
    name: {
      first:"Iphone",
      last:"5"
    },
    phone:"777",
    email:"bruce.wayne@gmail.com"
  }
];