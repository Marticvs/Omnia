const defaultState = {
  products: [],
  product: {name:{}},
  loading: false,
  errors:{}
}

export default (state=defaultState, action={}) => {
  switch (action.type) {
    case 'FETCH_PRODUCTS_FULFILLED': {
      return {
        ...state,
        products: action.payload.data.data,
        loading: false,
        errors: {}
      }
    }

    case 'FETCH_PRODUCTS_PENDING': {
      return {
        ...state,
        loading: true,
        errors: {}
      }
    }

    case 'FETCH_PRODUCTS_REJECTED': {
      return {
        ...state,
        loading: false,
        errors: { global: action.payload.message }
      }
    }

    case 'NEW_PRODUCT': {
      return {
        ...state,
        product: {name:{}}
      }
    }

    case 'SAVE_PRODUCTS_PENDING': {
      return {
        ...state,
        loading: true
      }
    }

    case 'SAVE_PRODUCTS_FULFILLED': {
      return {
        ...state,
        products: [...state.products, action.payload.data],
        errors: {},
        loading: false
      }
    }

    case 'SAVE_PRODUCT_REJECTED': {
      const data = action.payload.response.data;
      // convert feathers error formatting to match client-side error formatting
      const { "name.first":first, "name.last":last, phone, email } = data.errors;
      const errors = { global: data.message, name: { first,last }, phone, email };
      return {
        ...state,
        errors: errors,
        loading: false
      }
    }

    case 'FETCH_PRODUCT_PENDING': {
      return {
        ...state,
        loading: true,
        product: {name:{}}
      }
    }

    case 'FETCH_PRODUCT_FULFILLED': {
      return {
        ...state,
        product: action.payload.data,
        errors: {},
        loading: false
      }
    }

    case 'UPDATE_PRODUCT_PENDING': {
      return {
        ...state,
        loading: true
      }
    }

    case 'UPDATE_PRODUCT_FULFILLED': {
      const product = action.payload.data;
      return {
        ...state,
        products: state.products.map(item => item._id === product._id ? product : item),
        errors: {},
        loading: false
      }
    }

    case 'UPDATE_PRODUCT_REJECTED': {
      const data = action.payload.response.data;
      const { "name.first":first, "name.last":last, phone, email } = data.errors;
      const errors = { global: data.message, name: { first,last }, phone, email };
      return {
        ...state,
        errors: errors,
        loading: false
      }
    }

    case 'DELETE_PRODUCT_FULFILLED': {
      const _id = action.payload.data._id;
      return {
        ...state,
        products: state.products.filter(item => item._id !== _id)
      }
    }

    default:
      return state;
  }
}