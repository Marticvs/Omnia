import { combineReducers } from 'redux';
import ProductReducer from './product-reducer';
import { reducer as formReducer } from 'redux-form';

const reducers = {
  productStore: ProductReducer,
  form: formReducer
}

const rootReducer = combineReducers(reducers);

export default rootReducer;