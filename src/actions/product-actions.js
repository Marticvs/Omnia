import { client } from './';

const url = '/products';

export function fetchProducts(){
  return dispatch => {
    dispatch({
      type: 'FETCH_PRODUCTS',
      payload: client.get(url)
    })
  }
}

export function newProduct() {
  return dispatch => {
    dispatch({
      type: 'NEW_PRODUCT'
    })
  }
}

export function saveProduct(product) {
  return dispatch => {
    return dispatch({
      type: 'SAVE_PRODUCT',
      payload: client.post(url, product)
    })
  }
}

export function fetchProduct(_id) {
  return dispatch => {
    return dispatch({
      type: 'FETCH_PRODUCT',
      payload: client.get(`${url}/${_id}`)
    })
  }
}

export function updateProduct(product) {
  return dispatch => {
    return dispatch({
      type: 'UPDATE_PRODUCT',
      payload: client.put(`${url}/${product._id}`, product)
    })
  }
}

export function deleteProduct(_id) {
  return dispatch => {
    return dispatch({
      type: 'DELETE_PRODUCT',
      payload: client.delete(`${url}/${_id}`)
    })
  }
}