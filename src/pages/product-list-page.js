import React, { Component} from 'react';
import { connect } from 'react-redux';
import ProductList from '../components/product-list';
import { fetchProducts, deleteProduct } from '../actions/product-actions';

class ProductListPage extends Component {

  componentDidMount() {
    this.props.fetchProducts();
  }

  render() {
    return (
      <div>
        <h1>List of Products</h1>
        <ProductList products={this.props.products} deleteProduct={this.props.deleteProduct}/>
      </div>
    )
  }
}

// Make products  array available in  props
function mapStateToProps(state) {
  return {
       products : state.productStore.products,
      loading: state.productStore.loading,
      errors: state.productStore.errors
  }
}

export default connect(mapStateToProps, {fetchProducts, deleteProduct})(ProductListPage);