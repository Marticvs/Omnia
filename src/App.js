import React, { Component } from 'react';
import { NavLink, Route } from 'react-router-dom';
import { Container, Menu } from 'semantic-ui-react';
import ProductListPage from './pages/product-list-page';
import ProductFormPage from './pages/product-form-page';

class App extends Component {
  render() {
    return (
      <Container>
        <div className="ui two item menu">
          <NavLink className="item" activeClassName="active" exact to="/">
            Contacts List
          </NavLink>
          <NavLink className="item" activeClassName="active" exact to="/products/new">
            Add Product
          </NavLink>
        </div>
        <Route exact path="/" component={ProductListPage}/>
        <Route path="/products/new" component={ProductFormPage}/>
        <Route path="/products/edit/:_id" component={ProductFormPage}/>
      </Container>
    );
  }
}

export default App;