const assert = require('assert');
const app = require('../../src/app');

describe('\'product\' service', () => {
  it('registered the service', () => {
    const service = app.service('products'); // change product to products

    assert.ok(service, 'Registered the service');
  });
});