# omnia

> Test project - Marco Amodio

## Build Setup

``` bash
# install dependencies into omnia
npm install

# install dependencies into backend 
npm install

# start backend http://localhost:3000/products
yarn start

# start at http://localhost:3000/
yarn start


